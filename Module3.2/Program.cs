﻿using System;
using System.Collections.Generic;

namespace Module3_2
{
   static public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            while (true)
            {
                if (int.TryParse(input, out int i) && !input.Contains(',') && !input.Contains('.'))
                {
                    result = Convert.ToInt32(input);
                    return true;
                }
                else
                {
                    Console.WriteLine("You entered incorrect value! Please try again");

                    input = Console.ReadLine();

                }
            }
        }

        public int[] GetFibonacciSequence(int n)
        {
            TryParseNaturalNumber(Convert.ToString(n), out n);
            if(n == 0)
            {
                int[] result = new int[0];
                return result;
            }
            else if (n == 1)
            {
                int[] result = new int[1] { 0 };
                return result;
            }
            else if (n == 2)
            {
                int[] result = new int[2] { 0, 1 };
                return result;
            }
            else
            {
                int[] result = new int[n];
                result[0] = 0;
                result[1] = 1;
                for (int i = 2; i < result.Length; i++)
                {
                    result[i] = result[i - 1] + result[i - 2];
                }
                return result;
            }

        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            string sourceSt = Convert.ToString(Math.Abs(sourceNumber));


            char[] resultAr = new char[sourceSt.Length];

            for (int i = sourceSt.Length - 1; i >= 0; i--)
            {
                resultAr[sourceSt.Length - 1 - i] = sourceSt[i];
            }

            string resultSt = new string(resultAr);

            int result = Convert.ToInt32(resultSt);
            if (sourceNumber < 0)
            {
                return result * (-1);
            }
            return result;
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            while (true)
            {
                if (size < 0)
                {
                    Console.WriteLine("The size of array cant be negative");
                    size = Convert.ToInt32(Console.ReadLine());
                }
                else
                {
                    break;
                }

            }
            int[] result = new int[size];

            return result;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            int[] result = GenerateArray(source.Length);
            for (int i = 0; i < source.Length; i++)
            {
                result[i] = source[i] * -1;
            }
            return result;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            while (true)
            {
                if (size < 0)
                {
                    Console.WriteLine("The size of array cant be negative");
                    size = Convert.ToInt32(Console.ReadLine());
                }
                else
                {
                    break;
                }

            }
            int[] result = new int[size];

            return result;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> result = new List<int>();

            for (int i = 1; i < source.Length; i++)
            {
                if (source[i] > source[i - 1])
                {
                    result.Add(source[i]);
                }
            }
            return result;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int[,] result = new int[size, size];

            int height = size;
            int width = size;
            int counter = 1;
            int heightCounter = 0;
            int widthCounter = 0;

            for (int k = 1;k<=size ; k++)
            {
                for (int i = heightCounter; i < height;i++)
                {
                    if (i == heightCounter)
                    {
                        for (int j = widthCounter; j < width; j++)
                        {
                            result[i, j] = counter;

                            counter += 1;
                        }
                    }
                    else if (i == height - 1)
                    {
                        for (int j = width - 1; j >= widthCounter; j--)
                        {
                            result[i, j] = counter;

                            counter += 1;
                        }
                    }
                    else 
                    {
                        result[i, width - 1] = counter;

                        counter += 1;
                    }
                    
                }

                heightCounter += 1;

                height -= 1;

                for (int i = height - 1; i >= heightCounter; i--)
                {
                    result[i, widthCounter] = counter;

                    counter += 1;
                }
                width -= 1;
               
                widthCounter += 1;
            }
            return result;
        }
    }
}
